<?php

namespace Cwd\GenericBundle;

use Cwd\GenericBundle\DependencyInjection\Compiler\DoctrineEntityListenerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class CwdGenericBundle.
 *
 * @author  Ludwig Ruderstaller <lr@cwd.at>
 */
class CwdGenericBundle extends Bundle
{
    /**
     * @param ContainerBuilder $container
     */
    public function build(ContainerBuilder $container)
    {
        parent::build($container);
        #$container->addCompilerPass(new DoctrineEntityListenerPass());
    }
}
