<?php

/*
 * This file is part of tele.ring.
 *
 * (c)2014 Ludwig Ruderstaller <lr@cwd.at>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Cwd\GenericBundle\Controller;

use Cwd\GenericBundle\LegacyHelper\Utils;
use Rollerworks\Bundle\PasswordStrengthBundle\Validator\Constraints\PasswordStrength;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\Pbkdf2PasswordEncoder;
use Symfony\Component\Validator\Constraints\EqualTo;

/**
 * AuthController.
 *
 * @author  Ludwig Ruderstaller <lr@cwd.at>
 *
 * @Route("/auth")
 */
abstract class AuthController extends GenericController
{
    /**
     * Login Action.
     *
     * @param Request $request
     *
     * @Route("/login", name="auth_login")
     * @Method({"GET", "POST"})
     * @Template()
     *
     * @return array
     */
    public function loginAction(Request $request)
    {
        if ($request->attributes->has(Security::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(Security::AUTHENTICATION_ERROR);
        } else {
            $error = $request->getSession()->get(Security::AUTHENTICATION_ERROR);
        }

        return array(
            'last_username' => $request->getSession()->get(Security::LAST_USERNAME),
            'error' => $error,
        );
    }

    /**
     * Let a User update his profile.
     *
     * @param string $target
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function redirectProfileAction($target)
    {
        return $this->redirect($this->generateUrl($target, array('id' => $this->getUser()->getId())));
    }

    /**
     * @param string $passwd
     *
     * @Route("/passwd/{passwd}")
     * @Method({"GET"})
     *
     * @return array
     */
    public function passwdAction($passwd)
    {
        //echo password_hash($passwd, PASSWORD_BCRYPT, array("cost" => 13))."<br />";
        $encoder = new Pbkdf2PasswordEncoder('sha512', true, 1000, 40);
        $salt = Utils::generateRandomString(20);
        $password = $encoder->encodePassword($passwd, $salt);

        dump($salt, $password);

        return new Response(print_r(array('salt' => $salt, 'password' => $password)));
    }

    /**
     * Lostpassword Action.
     *
     * @param Request $request
     * @param string  $template
     *
     * @return array
     */
    public function handleLostpasswordAction(Request $request, $template)
    {
        if ('POST' === $request->getMethod()) {
            try {
                $user = $this->get('service_user')->findByEmail(filter_var($request->get('username'), FILTER_VALIDATE_EMAIL));
                $user->setPasswordtoken(\Cwd\GenericBundle\LegacyHelper\Utils::generateRandomString(32));
                $user->setPasswordtokencreated(new \DateTime());
                $this->get('service_user')->flush();

                $this->get('cwd.genericbundle.service.email')->sendLostPassword($user, $template);

                return array('success' => 'We sent you an email with further instructions');
            } catch (\Exception $e) {
                return array('error' => $e->getMessage());
            }
        }

        return array('error' => null);
    }

    /**
     * @param Request $request
     *
     * @Route("/setpassword/{token}")
     * @Template()
     * @Method({"GET", "POST"})
     *
     * @return array
     */
    public function setpasswordAction(Request $request)
    {
        try {
            $user = $this->get('service_user')->findByToken($request->get('token'));

            if ('POST' === $request->getMethod()) {
                $pwdStrength = new PasswordStrength(array('minStrength' => 2, 'minLength' => 6));

                $equal = new EqualTo(array('value' => $request->get('password_repeat')));
                $equal->message = 'Passwords do not match!';

                $errorList = $this->get('validator')->validate($request->get('password'), array($equal, $pwdStrength));

                if (count($errorList) == 0) {
                    $user->setPassword($request->get('password'))
                         ->setPasswordtoken(null)
                         ->setPasswordtokencreated(null);

                    $this->get('service_user')->flush();

                    return array('success' => 'Password successfully changed<br />You can now <a href="/admin/">login</a>');
                } else {
                    return array('token' => $request->get('token'), 'error' => $errorList[0]->getMessage());
                }
            } else {
                return array('token' => $request->get('token'));
            }
        } catch (\Exception $e) {
            return array('token' => 'asdf', 'error' => 'Token not found or no longer valid - '.$e->getMessage());
        }
    }

    /**
     * @Route("/login_check", name="auth_security_check")
     * @Method({"GET", "POST"})
     */
    public function securityCheckAction()
    {
        // The security layer will intercept this request
    }

    /**
     * @Route("/logout", name="auth_logout")
     * @Method({"GET"})
     */
    public function logoutAction()
    {
        // The security layer will intercept this request
    }
}
