<?php

/*
 * This file is part of the CWD Generic Bundle.
 *
 * (c)2014 Ludwig Ruderstaller <lr@cwd.at>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Cwd\GenericBundle\Controller;

use Cwd\GenericBundle\Options\ValidatedOptionsInterface;
use Cwd\GenericBundle\Options\ValidatedOptionsTrait;
use Oneup\AclBundle\Security\Acl\Manager\AclManager;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller as SymfonyController;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;

/**
 * Class Controller.
 *
 * @author  Ludwig Ruderstaller <lr@cwd.at>
 */
class GenericController extends SymfonyController implements ValidatedOptionsInterface
{
    use ValidatedOptionsTrait;

    /**
     * @var LoggerInterface
     */
    protected $logger = null;

    /**
     * @return AclManager
     */
    protected function getAclManager()
    {
        if (!$this->container->has('oneup_acl.manager')) {
            throw new ServiceNotFoundException('OneUp ACL Manager not set');
        }

        return $this->container->get('oneup_acl.manager');
    }

    /**
     * @return LoggerInterface
     */
    protected function getLogger()
    {
        if ($this->logger === null) {
            $this->logger = $this->get('logger');
        }

        return $this->logger;
    }

    /**
     * Session Flashmessenger.
     *
     * @param string $type
     * @param string $message
     */
    protected function flash($type = 'info', $message = null)
    {
        $this->get('session')->getFlashBag()->add(
            $type,
            $message
        );
    }

    /**
     * @param string $message
     */
    protected function flashInfo($message)
    {
        $this->flash('info', $message);
    }

    /**
     * @param string $message
     */
    protected function flashSuccess($message)
    {
        $this->flash('success', $message);
    }

    /**
     * @param string $message
     */
    protected function flashError($message)
    {
        $this->flash('error', $message);
    }
}
