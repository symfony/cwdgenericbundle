<?php

/*
 * This file is part of CWD Generic Bundle
 *
 * (c)2014 Ludwig Ruderstaller <lr@cwd.at>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Cwd\GenericBundle\Form\Subscriber;

use Cwd\GenericBundle\Form\Service\AutoRequire as AutoRequireService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

/**
 * Class AutoRequire.
 *
 * @author  Ludwig Ruderstaller <lr@cwd.at>
 */
class AutoRequire implements EventSubscriberInterface
{
    protected $service = null;

    /**
     * @param AutoRequireService $service
     */
    public function __construct(AutoRequireService $service)
    {
        $this->service = $service;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return array(FormEvents::POST_SET_DATA => array('onFormSetData', -10));
    }

    /**
     * @param FormEvent $event
     */
    public function onFormSetData(FormEvent $event)
    {
        /** @var Form $form */
        $form = $event->getForm();
        $this->service->process($this->getParent($form));
    }

    /**
     * @param Form|FormInterface $element
     *
     * @return \Symfony\Component\Form\Form
     */
    protected function getParent($element)
    {
        if (!$element->getParent()) {
            return $element;
        } else {
            return $this->getParent($element->getParent());
        }
    }
}
