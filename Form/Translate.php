<?php

/*
 * This file is part of the ACheck Bildungsplattform.
 *
 * (c) Ludwig Ruderstaller <lr@cwd.at>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Cwd\GenericBundle\Form;

use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Translation\Translator;

/**
 * Class Translatable Form.
 *
 * @author  Ludwig Ruderstaller <lr@cwd.at>
 * @DI\Service("form_translatable")
 */
abstract class Translate extends AbstractType
{
    /**
     * @DI\Inject("translator")
     *
     * @var Translator
     */
    public $translater;

    public function trans($label)
    {
        /** @var Translator $t */
        $t = $this->translater;
        if ($t instanceof Translator) {
            return $this->translater->trans($label);
        }

        return $label;
    }
}
