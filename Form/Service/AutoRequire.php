<?php

/*
 * This file is part of CWD Generic Bundle
 *
 * (c)2014 Ludwig Ruderstaller <lr@cwd.at>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Cwd\GenericBundle\Form\Service;

use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\Form\Form;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class AutoRequire.
 *
 * @author  Ludwig Ruderstaller <lr@cwd.at>
 * @DI\Service("cwd.generic.form.service.autorequire")
 */
class AutoRequire
{
    /**
     * @var ValidatorInterface
     */
    protected $validator;

    public $fields = array();

    protected $groups = null;

    /**
     * @param ValidatorInterface $validator
     *
     * @DI\InjectParams({
     *      "validator" = @DI\Inject("validator")
     * })
     */
    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    /**
     * Add a new form to processing queue.
     *
     * @param \Symfony\Component\Form\Form $form
     *
     * @return array
     */
    public function process(Form $form)
    {
        // no need to run for every field
        #if ($this->groups === null) {
            $this->groups = $this->getValidationGroups($form);
        #}

        // no need to run for every field
        #if (count($this->fields) == 0) {
            $this->fields = $this->getValidations($form, $this->groups);
        #}
    }

    /**
     * Get validation groups for the specified form.
     *
     * @param Form|FormInterface $form
     *
     * @return array|string
     */
    protected function getValidationGroups(Form $form)
    {
        $result = array('Default');
        $groups = $form->getConfig()->getOption('validation_groups');
        if (empty($groups)) {
            // Try to get groups from a parent
            if ($form->getParent()) {
                $result = $this->getValidationGroups($form->getParent());
            }
        } elseif (is_array($groups)) {
            // If groups is an array - return groups as is
            $result = $groups;
        } elseif ($groups instanceof \Closure) {
            $result = call_user_func($groups, $form);
        }

        return $result;
    }

    private function getValidations(Form $form, $groups)
    {
        $fields = array();

        $parent = $form->getParent();
        if ($parent && null !== $parent->getConfig()->getDataClass()) {
            $fields += $this->getConstraints($parent->getConfig()->getDataClass(), $groups);
        }

        if (null !== $form->getConfig()->getDataClass()) {
            $fields += $this->getConstraints($form->getConfig()->getDataClass(), $groups);
        }

        return $fields;
    }

    protected function getConstraints($obj, $groups)
    {
        $metadata = $this->validator->getMetadataFor($obj);
        $fields = array();
        foreach ($metadata->members as $elementName => $d) {
            $fields[$elementName] = false;
            $data = $d[0];
            foreach ($data->constraintsByGroup as $group => $constraints) {
                //dump(array($elementName, $group, $constraints));
                if (in_array($group, $groups)) {
                    foreach ($constraints as $constraint) {
                        if ($constraint instanceof NotBlank) {
                            $fields[$elementName] = true;
                            break 2;
                        }
                    }
                }
            }
        }

        return $fields;
    }

    /**
     * Gets metadata from system using the entity class name.
     *
     * @param string $className
     *
     * @return ClassMetadata
     * @codeCoverageIgnore
     */
    protected function getMetadataFor($className)
    {
        return $this->validator->getMetadataFactory()->getMetadataFor($className);
    }

    /**
     * Generate an Id for the element by merging the current element name
     * with all the parents names.
     *
     * @param Form $form
     *
     * @return string
     */
    protected function getElementId(Form $form)
    {
        /** @var Form $parent */
        $parent = $form->getParent();
        if (null !== $parent) {
            return $this->getElementId($parent).'_'.$form->getName();
        } else {
            return $form->getName();
        }
    }
}
