<?php

/*
 * This file is part of CWD Generic Bundle
 *
 * (c)2014 Ludwig Ruderstaller <lr@cwd.at>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Cwd\GenericBundle\Form\Extension;

use Cwd\GenericBundle\Form\Service\AutoRequire as AutoRequireService;
use Cwd\GenericBundle\Form\Subscriber\AutoRequire as AutoRequireSubscriber;
use JMS\DiExtraBundle\Annotation as DI;
use Monolog\Logger;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;

/**
 * Class AutoRequireExtension.
 *
 * @author  Ludwig Ruderstaller <lr@cwd.at>
 *
 * @DI\Service("cwd.generic.form.extension.autorequire")
 * @DI\Tag("form.type_extension", attributes={ "alias"="form" })
 * @internal Legacy for older Applications
 */
class AutoRequireExtension extends AbstractTypeExtension
{
    /**
     * @var AutoRequireService
     */
    protected $service;

    /**
     * @var bool
     */
    protected $enabled;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @param AutoRequireService $service
     * @param bool               $enabled
     * @param Logger             $logger
     *
     * @DI\InjectParams({
     *      "service" = @DI\Inject("cwd.generic.form.service.autorequire"),
     *      "enabled" = @DI\Inject("%cwd.genericbundle.form.extension.autorequire.enabled%"),
     *      "logger"  = @DI\Inject("logger")
     * })
     */
    public function __construct(AutoRequireService $service, $enabled = false, Logger $logger)
    {
        $this->service = $service;
        $this->enabled = $enabled;
        $this->logger = $logger;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if ($this->enabled) {
            $builder->addEventSubscriber(new AutoRequireSubscriber($this->service));
        }
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        if ($this->enabled) {
            //$this->logger->addDebug(print_r($this->service->fields, 1), $this->service->fields);
            if (isset($this->service->fields[$view->vars['name']])) {
                $view->vars['required'] = $this->service->fields[$view->vars['name']];
            }

            // Password Repeat Fallback
            if (($view->vars['name'] == 'first' || $view->vars['name'] == 'second') && isset($this->service->fields['password'])) {
                $view->vars['required'] = $this->service->fields['password'];
            }
        }
    }

    /**
     * Returns the name of the type being extended.
     *
     * @return string The name of the type being extended
     */
    public function getExtendedType()
    {
        return 'form';
    }
}
