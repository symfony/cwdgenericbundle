<?php

/*
 * This file is part of CWD Generic Bundle
 *
 * (c)2014 Ludwig Ruderstaller <lr@cwd.at>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Cwd\GenericBundle;

use Ali\DatatableBundle\Util\Datatable as AliDatatable;
use Doctrine\ORM\EntityManager;

/**
 * Class DataTable.
 *
 * @author  Ludwig Ruderstaller <lr@cwd.at>
 */
class DataTable extends AliDatatable
{
    /**
     * @param EntityManager $em
     */
    public function setEm(EntityManager $em)
    {
        $this->_em = $em;
    }

    public function getEm()
    {
        return $this->_em;
    }
}
