<?php

/*
 * This file is part of the CWD GenericBundle.
 *
 * (c)2014 Ludwig Ruderstaller <lr@cwd.at>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Cwd\GenericBundle\Grid;

use Ali\DatatableBundle\Util\Datatable;

/**
 * Class Grid.
 *
 * @author  Ludwig Ruderstaller <lr@cwd.at>
 */
class Grid
{
    /**
     * @var Datatable
     */
    protected $datatable;

    /**
     * @return Datatable
     */
    public function getDatatable()
    {
        return $this->datatable;
    }

    /**
     * @param Datatable $datatable
     *
     * @return $this
     */
    public function setDatatable(Datatable $datatable)
    {
        $this->datatable = $datatable;

        return $this;
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function execute()
    {
        return $this->getDatatable()->execute();
    }
}
