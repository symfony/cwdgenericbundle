<?php

namespace Cwd\GenericBundle\Tests\Options;

use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ValidatedOptionsTraitTest extends \PHPUnit_Framework_TestCase
{
    public function testGetOptionCallsSetOptionsOnce()
    {
        $impl = $this->getMock('Cwd\GenericBundle\Tests\Options\ValidatedOptionsTraitImpl', array(
            'setOptions',
            'configureOptions',
        ));

        $impl
            ->expects($this->once())
            ->method('setOptions')
            ->willReturn(array('required1' => 'bar'))
        ;
        $impl
            ->expects($this->once())
            ->method('configureOptions')
            ->with($this->callback(function (OptionsResolverInterface $resolver) {
                $resolver->setRequired(array(
                    'required1',
                ));

                return $resolver instanceof OptionsResolver;
            }))
        ;

        $this->assertSame('bar', $impl->getOption('required1'));
        $this->assertSame('bar', $impl->getOption('required1'));
    }

    /**
     * @expectedException Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     */
    public function testGetOptionCallsResolverAndFailsOnRequiredOption()
    {
        $impl = $this->getMock('Cwd\GenericBundle\Tests\Options\ValidatedOptionsTraitImpl', array(
            'setOptions',
            'configureOptions',
        ));

        $impl
            ->expects($this->once())
            ->method('setOptions')
            ->willReturn(array())
        ;
        $impl
            ->expects($this->once())
            ->method('configureOptions')
            ->with($this->callback(function (OptionsResolverInterface $resolver) {
                $resolver->setRequired(array(
                    'required1',
                ));

                return $resolver instanceof OptionsResolver;
            }))
        ;

        $impl->getOption('required1');
    }

    public function testConfigureOptionsIsCalledAndConfiguresResolver()
    {
        $impl = $this->getMock('Cwd\GenericBundle\Tests\Options\ValidatedOptionsTraitImpl', array(
            'setOptions',
            'configureOptions',
        ));

        $impl
            ->expects($this->once())
            ->method('setOptions')
            ->willReturn(array('required1' => 'bar'))
        ;

        $impl
            ->expects($this->once())
            ->method('configureOptions')
            ->with($this->callback(function (OptionsResolverInterface $resolver) {
                $resolver->setRequired(array(
                    'required1',
                ));
                $resolver->setDefaults(array(
                    'new_default' => 'baz',
                ));

                return $resolver instanceof OptionsResolver;
            }))
        ;

        $this->assertSame('bar', $impl->getOption('required1'));
        $this->assertSame('baz', $impl->getOption('new_default'));
    }

    /**
     * @expectedException Cwd\GenericBundle\Exception\InvalidOptionException
     */
    public function testGetOptionThrowsExceptionOnInvalidOption()
    {
        $impl = $this->getMock('Cwd\GenericBundle\Tests\Options\ValidatedOptionsTraitImpl', array(
            'setOptions',
            'configureOptions',
        ));

        $impl
            ->expects($this->once())
            ->method('setOptions')
            ->willReturn(array('required1' => 'bar'))
        ;
        $impl
            ->expects($this->once())
            ->method('configureOptions')
            ->with($this->callback(function (OptionsResolverInterface $resolver) {
                $resolver->setRequired(array(
                    'required1',
                ));

                return $resolver instanceof OptionsResolver;
            }))
        ;

        $impl->getOption('doesNotExist');
    }

    public function testGetOptionOrDefaultReturnsValueOrDefault()
    {
        $impl = $this->getMock('Cwd\GenericBundle\Tests\Options\ValidatedOptionsTraitImpl', array(
            'setOptions',
            'configureOptions',
        ));

        $impl
            ->expects($this->once())
            ->method('setOptions')
            ->willReturn(array('required1' => 'bar'))
        ;
        $impl
            ->expects($this->once())
            ->method('configureOptions')
            ->with($this->callback(function (OptionsResolverInterface $resolver) {
                $resolver->setRequired(array(
                    'required1',
                ));

                return $resolver instanceof OptionsResolver;
            }))
        ;

        $this->assertSame('bar', $impl->getOptionOrDefault('required1', 'baz'));
        $this->assertSame('baz', $impl->getOptionOrDefault('doesNotExist', 'baz'));
        $this->assertSame(null, $impl->getOptionOrDefault('doesNotExist'));
    }

    public function testWorksWithoutCallingSetOptionsIfNothingIsRequired()
    {
        $impl = $this->getMock('Cwd\GenericBundle\Tests\Options\ValidatedOptionsTraitImpl', null);

        $this->assertSame('baz', $impl->getOptionOrDefault('new_default', 'baz'));
    }
}
