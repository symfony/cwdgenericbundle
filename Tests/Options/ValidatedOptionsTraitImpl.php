<?php

namespace Cwd\GenericBundle\Tests\Options;

use Cwd\GenericBundle\Options\ValidatedOptionsTrait;

/**
 * This class is required for ValidatedOptionsTrait testing.
 *
 * @see http://blog.florianwolters.de/educational/2012/09/20/Testing-Traits-with-PHPUnit/
 *
 * @author David Herrmann <office@web-emerge.com>
 */
class ValidatedOptionsTraitImpl
{
    use ValidatedOptionsTrait;
}
