<?php

/*
 * This file is part of cwd generic bundle
 *
 * (c)2014 Ludwig Ruderstaller <lr@cwd.at>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Cwd\GenericBundle\Evaluators;

use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class RequestAccessEvaluator.
 *
 * @author Ludwig Ruderstaller <lr@cwd.at>
 * @DI\Service
 */
class IsMe
{
    /**
     * @var TokenStorage
     */
    private $tokenStorage;

    /**
     * @param ContainerInterface $container
     *
     * @DI\InjectParams({
     *     "tokenStorage" = @DI\Inject("security.token_storage"),
     * })
     */
    public function __construct(TokenStorage $tokenStorage)
    {
        $this->tokenStorage;
    }

    /**
     * @param UserInterface $user
     *
     * @DI\SecurityFunction("isMe")
     *
     * @return bool
     */
    public function isMe(UserInterface $user)
    {
        return $user->getId() == $this->tokenStorage->getToken()->getUser()->getId();
    }
}
