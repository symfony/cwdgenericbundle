<?php

/*
 * This file is part of vorsaetze.telering.at.
 *
 * (c)2014 Ludwig Ruderstaller <lr@cwd.at>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Cwd\GenericBundle\LegacyHelper;

/**
 * Class Utils.
 *
 * @author  Ludwig Ruderstaller <lr@cwd.at>
 */
class Utils
{
    /**
     * Generates random string with given length.
     *
     * @param int $length
     *
     * @return string
     */
    public static function generateRandomString($length = 10)
    {
        $random = '';
        srand((double) microtime() * 1000000);
        $charList = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charList .= 'abcdefghijklmnopqrstuvwxyz';
        $charList .= '1234567890';

        for ($i = 0; $i < $length; ++$i) {
            $random .= substr($charList, (rand() % (strlen($charList))), 1);
        }

        return $random;
    }
}
