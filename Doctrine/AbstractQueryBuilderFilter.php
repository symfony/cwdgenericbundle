<?php
/*
 * This file is part of cwd generic bundle
 *
 * (c)2015 Ludwig Ruderstaller <lr@cwd.at>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Cwd\GenericBundle\Doctrine;
use Cwd\GenericBundle\LegacyHelper\Utils;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Security\Core\Util\SecureRandom;

/**
 * Class QueryBuilderFilterTrait
 *
 * @package Cwd\GenericBundle\Doctrine\Traits
 * @author  Ludwig Ruderstaller <lr@cwd.at>
 *
 *
 */
abstract class AbstractQueryBuilderFilter extends EntityRepository
{
    /**
     * @var array
     */
    protected $joins = array();

    /**
     * @var string
     */
    protected $alias = 'object';

    /**
     * @var array
     */
    protected $joinMap = array();

    /**
     * @var int
     */
    protected $paramCount = 1;

    /**
     * @param array $filters
     * @param null  $qb
     *
     * @return QueryBuilder|null
     */
    public function queryBuilderFilter($filters = array(), $qb = null)
    {
        if ($qb === null) {
            $qb = $this->createQueryBuilder($this->alias);
        }

        $this->parseFilter($filters, $qb);

        return $qb;
    }

    /**
     * @param array        $filters
     * @param QueryBuilder $qb
     * @param Expr         $expr
     *
     * @throws \Exception
     *
     * @return QueryBuilder
     */
    protected function parseFilterBackup($filters, $qb, $expr = null)
    {
        if (isset($filters->condition)) {

            if ($filters->condition == 'AND') {
                $group = $qb->expr()->andX();
                $this->parseFilter($filters->rules, $qb, $group);
                $qb->andWhere($group);
            } else {
                $group = $qb->expr()->orX();
                $this->parseFilter($filters->rules, $qb, $group);
                $qb->orWhere($group);
            }
            dump("upper ".$filters->condition);
            dump($group);
        } else {
            foreach ($filters as $key => $filter) {
                if (isset($filter->id)) {
                    $fieldExpr = $this->getRule($filter, $qb);
                    $expr->add($fieldExpr);
                } elseif (isset($filter->condition)) {
                    if ($filter->condition == 'AND') {
                        $group = $qb->expr()->andX();
                        $this->parseFilter($filter->rules, $qb, $group);
                        $qb->andWhere($group);
                    } else {
                        $group = $qb->expr()->orX();
                        $this->parseFilter($filter->rules, $qb, $group);
                        $qb->orWhere($group);
                    }
                    dump("lower ".$filter->condition);
                    dump($group);
                }
            }
        }
    }

    /**
     * @param      $filters
     * @param QueryBuilder $qb
     * @param Expr\Andx|Expr\OrX|null $expr
     *
     * @return mixed
     * @throws \Exception
     */
    protected function parseFilter($filters, $qb, $expr = null)
    {
        if (isset($filters->condition)) {
            if ($filters->condition == 'AND') {
                $group = $qb->expr()->andX();
                $this->parseFilter($filters->rules, $qb, $group);
                if ($expr === null) {
                    $qb->andWhere($group);
                } else {
                    $expr->add($group);
                }
            } else {
                $group = $qb->expr()->orX();
                $this->parseFilter($filters->rules, $qb, $group);
                if ($expr === null) {
                    $qb->orWhere($group);
                } else {
                    $expr->add($group);
                }
            }
        } elseif (isset($filters->id)) {
            $fieldExpr = $this->getRule($filters, $qb);
            $expr->add($fieldExpr);
            $group = $expr;
        } elseif (is_array($filters)) {
            foreach ($filters as $key => $filter) {
                $group = $this->parseFilter($filter, $qb, $expr);
            }
        }

        return $group;
    }

    /**
     * @param \stdClass    $filter
     * @param QueryBuilder $qb
     *
     * @return Expr
     * @throws \Exception
     */
    protected function getRule($filter, $qb)
    {
        $field = $filter->field;
        $value = $filter->value;
        $table = current(explode('.', $field));
        $param = $this->paramCount++;


        if (!isset($this->joins[$table]) && $table != $this->alias) {
            $this->joins[$table] = true;
            if (isset($this->joinMap[$table])) {
                $tableField = $this->joinMap[$table];
            } else {
                $tableField = $table;
            }
            $qb->leftJoin($this->alias . '.' . $tableField, $table);
        }

        if ($filter->type == 'boolean') {
            $value = ($filter->value == 'true' ? 1 : 0);
        }

        switch ($filter->operator) {
            case 'contains':
                $expr = $qb->expr()->like($field, '?'.$param);
                $qb->setParameter($param, '%'.$value.'%');
                break;
            case 'not_contains':
                $expr = $qb->expr()->notLike($field, '?'.$param);
                $qb->setParameter($param, '%'.$value.'%');
                break;
            case 'equal':
                $expr = $qb->expr()->eq($field, '?'.$param);
                $qb->setParameter($param, $value);
                break;
            case 'not_equal':
                $expr = $qb->expr()->neq($field, '?'.$param);
                $qb->setParameter($param, $value);
                break;
            case 'begins_with':
                $expr = $qb->expr()->like($field, '?'.$param);
                $qb->setParameter($param, $value.'%');
                break;
            case 'not_begins_with':
                $expr = $qb->expr()->notLike($field, '?'.$param);
                $qb->setParameter($param, $value.'%');
                break;
            case 'ends_with':
                $expr = $qb->epxr()->like($field, '?'.$param);
                $qb->setParameter($param, '%'.$value);
                break;
            case 'not_ends_with':
                $expr = $qb->expr()->notLike($field, '?'.$param);
                $qb->setParameter($param, '%'.$value);
                break;
            case 'in':
                $expr = $qb->expr()->in($field, '?'.$param);
                $qb->setParameter($param, (is_array($value) ? $value : array($value)));
                break;
            case 'not_in':
                $expr = $qb->expr()->notIn($field, '?'.$param);
                $qb->setParameter($param, (is_array($value) ? $value : array($value)));
                break;
            case 'less':
                $expr = $qb->expr()->lt($field, '?'.$param);
                $qb->setParameter($param, $value);
                break;
            case 'less_or_equal':
                $expr = $qb->expr()->lte($field, '?'.$param);
                $qb->setParameter($param, $value);
                break;
            case 'greater':
                $expr = $qb->expr()->gt($field, '?'.$param);
                $qb->setParameter($param, $value);
                break;
            case 'greater_or_equal':
                $expr = $qb->expr()->gte($field, '?'.$param);
                $qb->setParameter($param, $value);
                break;
            case 'between':
                $expr = $qb->expr()->between($field, '?'.$param."1", '?'.$param."2");
                $qb->setParameter($param."1", $value[0]);
                $qb->setParameter($param."2", $value[1]);
                break;
            case 'is_empty':
                $expr = $qb->expr()->eq($field, '?'.$param);
                $qb->setParameter($param, '');
                break;
            case 'is_not_empty':
                $expr = $qb->expr()->neq($field, '?'.$param);
                $qb->setParameter($param, '');
                break;
            case 'is_null':
                $expr = $qb->expr()->isNull($field);
                break;
            case 'is_not_null':
                $expr = $qb->expr()->isNotNull($field);
                break;
            default:
                throw new \Exception(sprintf('Unknown operator %s', $filter->operator));
        }

        return $expr;
    }
}
