<?php
/*
 * This file is part of CWD Generic Bundle.
 *
 * (c)2015 Ludwig Ruderstaller <lr@cwd.at>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Cwd\GenericBundle\Doctrine;

use Gedmo\Sluggable\SluggableListener as BaseSluggableListener;

/**
 * Class SluggableListener
 *
 * @package Cwd\GenericBundle\Doctrine
 * @author  Ludwig Ruderstaller <lr@cwd.at>
 */
class SluggableListener extends BaseSluggableListener
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->setTransliterator(array('\Cwd\GenericBundle\Doctrine\UmlautTransliterator', 'transliterate'));
    }
}
