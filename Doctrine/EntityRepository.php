<?php

/*
 * This file is part of the WienHolding IT Assets Tool.
 *
 * (c)2014 Ludwig Ruderstaller <lr@cwd.at>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Cwd\GenericBundle\Doctrine;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository as DoctrineEntityRepository;

/**
 * Class EntityRepository.
 *
 * @author  Ludwig Ruderstaller <lr@cwd.at>
 */
abstract class EntityRepository extends DoctrineEntityRepository
{
    /**
     * Find Rows based on ID.
     *
     * @param array $ids
     *
     * @return ArrayCollection
     */
    public function findByArray(array $ids)
    {
        if (count($ids) == 0) {
            return new ArrayCollection();
        }

        $q = $this
            ->createQueryBuilder('i')
            ->select('i')
            ->where('i.id IN(:id)')
            ->setParameter('id', $ids)
            ->getQuery();

        return $q->getResult();
    }

    /**
     * @param string $slug
     *
     * @return misc
     */
    public function findBySlug($slug)
    {
        $q = $this->createQueryBuilder('c')
            ->select('c')
            ->andWhere('c.slug = :slug')
            ->setParameter('slug', $slug)
            ->getQuery();

        return $q->getSingleResult();
    }
}
