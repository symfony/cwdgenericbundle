<?php

/*
 * This file is part of the CWD GenericBundle.
 *
 * (c)2014 Ludwig Ruderstaller <lr@cwd.at>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Cwd\GenericBundle\Menu;

use Knp\Menu\ItemInterface;
use Knp\Menu\Matcher\Voter\VoterInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class RequestVoter implements VoterInterface
{
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function matchItem(ItemInterface $item)
    {
        if ($item->getUri() === $this->container->get('request')->getRequestUri()) {
            return true;
        } else {
            if ($item->getUri() !== '/' && (substr(
                        $this->container->get('request')->getRequestUri(),
                        0,
                        strlen($item->getUri())
                    ) === $item->getUri())
            ) {
                return true;
            }
        }

        return;
    }
}
