<?php

namespace Cwd\GenericBundle\Exception;

class ServiceException extends BaseException
{
    public function __construct($message = '', $code = 0, \Exception $e = null)
    {
        return parent::__construct($message, $code, $e);
    }
}
