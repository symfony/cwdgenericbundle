<?php

/*
 * This file is part of cwd Generic Bundle
 *
 * (c)2014 Ludwig Ruderstaller <lr@cwd.at>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Cwd\GenericBundle\Service;

use Cwd\GenericBundle\Exception\PersistanceException;
use Doctrine\ORM\EntityManager;
use JMS\DiExtraBundle\Annotation as DI;
use Monolog\Logger;

/**
 * Class Generic.
 *
 * @author  Ludwig Ruderstaller <lr@cwd.at>
 *
 * @DI\Service("cwd.generic.service.generic")
 */
abstract class Generic
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @param EntityManager $entityManager
     * @param Logger        $logger
     */
    public function __construct(EntityManager $entityManager, Logger $logger)
    {
        $this->entityManager = $entityManager;
        $this->logger = $logger;
    }

    /**
     * @param int    $id
     * @param string $model
     *
     * @return object
     *
     * @deprecated Use findById instead!
     */
    public function findByModel($id, $model)
    {
        return $this->findById($model, $id);
    }

    /**
     * Find All by Model.
     *
     * @param string $model
     * @param array  $filter
     * @param array  $order
     * @param int    $amount
     * @param int    $offset
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function findAllByModel($model, $filter = array(), $order = array(), $amount = 10, $offset = 0)
    {
        return $this->getEm()->getRepository($model)->findBy($filter, $order, $amount, $offset);
    }

    /**
     * @param string $model
     * @param array  $where
     *
     * @return int
     */
    public function getCountByModel($model, $where = array())
    {
        $qb = $this->getEm()->createQueryBuilder();
        $qb->select($qb->expr()->count('x'))
            ->from($model, 'x');

        if (count($where) > 0) {
            $qb->andWhere($where);
        }

        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * @param string $model
     * @param int    $id
     *
     * @return object
     */
    public function findById($model, $id)
    {
        return $this->entityManager->getRepository($model)->find($id);
    }

    /**
     * Find Entities by fields in given Model.
     *
     * @param string $model
     * @param array  $filter
     * @param array  $sort
     * @param int    $limit
     * @param int    $offset
     *
     * @return array
     */
    public function findByFilter($model, $filter = array(), $sort = array(), $limit = null, $offset = null)
    {
        return $this->getEm()->getRepository($model)->findby($filter, $sort, $limit, $offset);
    }

    /**
     * Find one Entities by fields in given Model.
     *
     * @param string $model
     * @param array  $filter
     *
     * @return object
     */
    public function findOneByFilter($model, $filter = array())
    {
        return $this->getEm()->getRepository($model)->findOneby($filter);
    }

    /**
     * @return Logger
     */
    public function getLogger()
    {
        return $this->logger;
    }

    /**
     * @param $object
     *
     * @throws PersistanceException
     *
     * @return bool
     */
    public function persist($object)
    {
        try {
            $this->entityManager->persist($object);
        } catch (\Exception $e) {
            $this->getLogger()->warn('Object could not be saved', (array) $e);
            throw new PersistanceException('Object could not be stored - '.$e->getMessage(), null, $e);
        }

        return true;
    }

    /**
     * Flush EntityManager.
     */
    public function flush()
    {
        $this->getEm()->flush();
    }

    /**
     * @param misc $id
     *
     * @throws NotFoundException
     *
     * @return true
     */
    public function remove($id)
    {
        if (is_int($id)) {
            $object = $this->find($id);
        } else {
            $object = $id;
        }
        $this->getEm()->remove($object);
        $this->getEm()->flush();

        return true;
    }

    /**
     * @param int $id
     *
     * @throws NotFoundException
     *
     * @return true
     */
    public function restore($id)
    {
        $this->getEm()->getFilters()->disable('softdeleteable');
        $object = $this->find($id);
        $object->setDeletedAt(null);
        $this->getEm()->flush();

        return true;
    }

    /**
     * @return EntityManager
     */
    public function getEm()
    {
        return $this->entityManager;
    }

    /**
     * Find Object by ID, throwing an optional exception if it is not found.
     *
     * @param int $pid               Entity ID
     * @param string $modelName      Model definition (class name or alias) to use
     * @param string $exceptionClass Optional Exception class to throw when nothing is found
     *
     * @return mixed
     * @throws NotFoundException
     */
    public function findOneByIdForModel($pid, $modelName, $exceptionClass = null)
    {
        try {
            $obj = $this->findById($modelName, intval($pid));

            if ($obj === null) {
                $this->getLogger()->info('Row with ID {id} not found', array('id' => $pid));
                if (null !== $exceptionClass) {
                    throw new $exceptionClass('Row with ID ' . $pid . ' not found');
                }
            }

            return $obj;
        } catch (\Exception $e) {
            if (null !== $exceptionClass) {
                throw new $exceptionClass();
            }
        }
    }
}
