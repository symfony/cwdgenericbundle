<?php

/*
 * This file is part of vorsaetze.telering.at.
 *
 * (c)2014 Ludwig Ruderstaller <lr@cwd.at>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Cwd\GenericBundle\Service;

use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class Email.
 *
 * @author  Ludwig Ruderstaller <lr@cwd.at>
 *
 * @DI\Service("cwd.genericbundle.service.email")
 */
class Email
{
    /**
     * @var \Swift_Mailer
     */
    protected $mailer;

    /**
     * @var \Twig_Environment
     */
    protected $twig;

    /**
     * @var string|null
     */
    protected $maildomain = null;

    /**
     * @var string|null
     */
    protected $fromEmail = null;

    /**
     * @var string|null
     */
    protected $fromName = null;

    /**
     * @param \Swift_Mailer     $mailer
     * @param \Twig_Environment $twig
     * @param string            $fromEmail
     * @param string            $fromName
     *
     * @DI\InjectParams({
     *      "mailer" = @DI\Inject("mailer"),
     *      "twig"   = @DI\Inject("twig"),
     *      "fromEmail" = @DI\Inject("%cwd.genericbundle.from.email%"),
     *      "fromName" = @DI\Inject("%cwd.genericbundle.from.name%"),
     * })
     */
    public function __construct(\Swift_Mailer $mailer, \Twig_Environment $twig, $fromEmail = null, $fromName = null)
    {
        $this->mailer = $mailer;
        $this->twig = $twig;
        $this->fromEmail = $fromEmail;
        $this->fromName = $fromName;
    }

    /**
     * Send password reset link.
     *
     * @param UserInterface $user
     * @param string        $template
     */
    public function sendLostPassword(UserInterface $user, $template)
    {
        $subject = 'Password Änderung wurde beantragt';
        $templateContent = $this->twig->loadTemplate($template);
        $body = $templateContent->render(array(
            'user' => $user,
            'subject' => $subject,
        ));

        $this->sendMail($body, $subject, $user);
    }

    /**
     * Send Mail to User.
     *
     * @param string     $body
     * @param string     $subject
     * @param UserEntity $user
     */
    protected function sendMail($body, $subject, UserInterface $user)
    {
        $message = \Swift_Message::newInstance()
            ->setFrom($this->fromEmail, $this->fromName)
            ->setSubject($subject)
            ->setTo($user->getUsername(), $user->getName())
            ->setBody($body, 'text/html', 'UTF-8');

        $this->mailer->send($message);
    }
}
