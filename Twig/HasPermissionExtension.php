<?php

/*
 * This file is part of password-manager.
 *
 * (c)2014 Ludwig Ruderstaller <lr@cwd.at>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */

namespace Cwd\GenericBundle\Twig;

use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManager;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class HasPermission Extension.
 *
 * Calls the Decision Manager for given User
 *
 * @author  Ludwig Ruderstaller <lr@cwd.at>
 *
 * @DI\Service("cwd.genericbundle.twig.haspermission_extension")
 * @DI\Tag("twig.extension")
 */
class HasPermissionExtension extends \Twig_Extension
{
    /**
     * @var AccessDecisionManager
     */
    protected $decisionManager;

    /**
     * @var TokenStorage
     */
    protected $securityContext;

    /**
     * @param AccessDecisionManagerInterface $manager
     * @param TokenStorage                   $token
     *
     * @DI\InjectParams({
     *      "manager" = @DI\Inject("security.access.decision_manager"),
     *      "token" = @DI\Inject("security.token_storage")
     * })
     */
    public function __construct(AccessDecisionManagerInterface $manager, TokenStorage $token)
    {
        $this->decisionManager = $manager;
        $this->securityContext = $token;
    }

    /**
     * Check permission for given User and Object.
     *
     * @param string|array  $permission
     * @param misc          $object
     * @param UserInterface $user
     *
     * @return bool
     */
    public function hasPermission($permission, $object = null, UserInterface $user = null)
    {
        if ($user !== null) {
            $token = new UsernamePasswordToken($user, 'none', 'none', $user->getRoles());
        } else {
            $token = $this->securityContext->getToken();
        }

        if (!is_array($permission)) {
            $permission = array($permission);
        }

        if ($object !== null) {
            $oid = new ObjectIdentity('class', $object);
        }

        return $this->decisionManager->decide($token, $permission, $oid);
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('has_permission', array($this, 'hasPermission')),
        );
    }

    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return 'haspermission_extension';
    }
}
