<?php

namespace Cwd\GenericBundle\Twig;

class GravatarExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('gravatar', array($this, 'gravatar')),
        );
    }

    public function gravatar($email, $size = 40)
    {
        return 'https://www.gravatar.com/avatar/'.
            md5(strtolower(trim($email))).
            '?d=mm'.
            '&s='.$size;
    }

    public function getName()
    {
        return 'gravatar_extension';
    }
}
