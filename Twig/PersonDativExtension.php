<?php

/*
 * This file is part of cwd - generic bundle
 *
 * (c)2014 Ludwig Ruderstaller <lr@cwd.at>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */

namespace Cwd\GenericBundle\Twig;

use JMS\DiExtraBundle\Annotation as DI;

/**
 * Class PersonDativ Extension.
 *
 * @author  Ludwig Ruderstaller <lr@cwd.at>
 *
 * @DI\Service("cwd.genericbundle.twig.persondativ_extension")
 * @DI\Tag("twig.extension")
 */
class PersonDativExtension extends \Twig_Extension
{
    /**
     * @return array
     */
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('dativ', array($this, 'dativ')),
        );
    }

    /**
     * @param string $name
     * @param string $language
     *
     * @see http://www.canoo.net/services/OnlineGrammar/InflectionRules/FRegeln-N/FKlassen/Name-Person1.html?MenuId=Word11114
     *
     * @return string
     */
    public function dativ($name, $language = 'de')
    {
        if ($language == 'de') {
            $e = substr($name, -1);
            if (in_array($e, array('s', 'x', 'z'))) {
                return $name."'";
            }

            return $name.'s';
        }
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'person_dativ_extension';
    }
}
